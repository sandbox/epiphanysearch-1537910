Drupal TorFX Currency Ticker Plugin 

Unzip the plugin and put the folder �torfx_ct� in sites\all\modules\. In the admin area go to �Modules� and enable �TorFX Currency Ticker

Got to structure\blocks to place the module on your page

The module will accept two parameters $style and $from.  An option for �torfx currency ticker� will appear in the administration menu, this is where you can change the default values. You may need to clear the cache to get this to appear

Style � light, dark, orange
From will accept the following three letter country codes
'GBP' => 'UK Pounds',
'USD' => 'US Dollar',
'EUR' => 'Euro',
'AUD' => 'Australian Dollar',
'CAD' => 'Canadian Dollar',
'CZK' => 'Czech Koruna',
'DKK' => 'Danish Krone',
'HKD' => 'Hong Kong Dollar',
'HUF' => 'Hungarian Forint',
'INR' => 'Indian Rupee',
'JPY' => 'Japanese Yen',
'NZD' => 'New Zealand Dollar',
'NOK' => 'Norwegian Krone',
'SGD' => 'Singapore Dollar',
'ZAR' => 'South African Rand',
'SEK' => 'Swedish Kroner',
'CHF' => 'Swiss Franc',
'THB' => 'Thai Baht',
'TRY' => 'Turkish Lira',
'AED' => 'UAE Dirham'

If no parameters are entered the function will default to style=light and from=GBP
